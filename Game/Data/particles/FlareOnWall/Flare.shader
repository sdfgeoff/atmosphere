shader_type spatial;
render_mode blend_add,depth_draw_opaque,cull_back,unshaded;
uniform vec4 color : hint_color;
uniform float fade : hint_range(0.0, 1.0);

varying float factor;

void vertex(){
	vec4 vert_world = WORLD_MATRIX * vec4(VERTEX, 1.0);
	vec3 cam_local = (INV_CAMERA_MATRIX * vert_world).xyz;
	cam_local = normalize(cam_local);
	vec3 cam_normal = (MODELVIEW_MATRIX * vec4(NORMAL, 0.0)).xyz;
	factor = dot(cam_normal, cam_local);
}

void fragment() {
	float depth_tex = textureLod(DEPTH_TEXTURE,SCREEN_UV,0.0).r;
	vec4 world_pos = INV_PROJECTION_MATRIX * vec4(SCREEN_UV*2.0-1.0,depth_tex*2.0-1.0,1.0);
	world_pos.xyz/=world_pos.w;
	float wall = clamp(1.0 - smoothstep(world_pos.z+0.5,world_pos.z,VERTEX.z),0.0,1.0);
	
	
	float falloff = pow((factor * 1.1), 16.0) * wall;
	ALBEDO = fade * (falloff * color.rgb + falloff);
}
