extends Spatial

var launchers = []
var counter = 0
func _ready():
	self.launchers = []
	for child in self.get_children():
		if child.has_method("fire"):  # If you can shoot it, it's a gun
			self.launchers.append(child)
	self.counter = 0
			

func fire():
	if launchers[counter].can_fire():
		launchers[counter].fire()
		counter += 1
		if counter > len(launchers) - 1:
			counter = 0
	