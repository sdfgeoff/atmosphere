# This file is run inside blender, it opens each of the passed in files and
# exports it to the type specified in the render output panel

import fileinput
import bpy
import argparse
import sys
import os

import logging

import time

ESCN_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "godot-blender-exporter"
)
sys.path = [ESCN_PATH] + sys.path  # Ensure exporter from this folder

from io_scene_godot import export_godot


def export_escn(base_dir, out_file):
	"""Fake the export operator call"""
	class op:
		def __init__(self):
			self.report = print
			
	res = export_godot.save(op(), bpy.context, out_file, 
		object_types={"EMPTY", "CAMERA", "LAMP", "ARMATURE", "MESH", "CURVE"},
		use_active_layers=False,
		use_export_selected=False,
		use_mesh_modifiers=True,
        material_search_paths='PROJECT_DIR',
        camera_keep_width=False,
	)
    


def export_dae(base_dir, out_file):
    bpy.ops.wm.collada_export(
        filepath=out_file,
        apply_modifiers=True,
        use_texture_copies=False
    )

    # TODO: Fix collada paths to be relative to the file
    return []

def export_obj(base_dir, out_file):
    bpy.ops.export_scene.obj(filepath=out_file)
    os.remove(out_file.replace('.obj', '.mtl'))  # AFAIK Godot ignores .mtl files
    return []

def export_png(base_dir, out_file):
    bpy.context.scene.render.filepath = out_file
    bpy.ops.render.render(write_still=True)
    return []


FUNCT_DICT = {
    'escn':(export_escn, '.escn'),
    'png':(export_png, '.png'),
    'dae':(export_dae, '.dae'),
    'obj':(export_obj, '.obj'),
}

def export_blend(base_dir, filename):
    bpy.ops.wm.open_mainfile(filepath=filename)
    export_type = bpy.context.scene.render.filepath
    if export_type == 'skip':
        return []
        
    function, outfile = get_instructions(filename, export_type)
    output_files = [outfile]
    if check_should_build(filename, outfile):
        logging.info("Building {} into {}".format(filename, outfile))
        extra_files = function(base_dir, outfile)
        if extra_files is not None:
            output_files += output_files
        logging.info("Built")
    else:
        logging.info("Skipping {}".format(filename))

    return output_files
            
def check_should_build(in_filename, out_filename):
    if not os.path.exists(out_filename):
        return True
        
    last_build_time = os.path.getmtime(out_filename)
    build = last_build_time < os.path.getmtime(in_filename)
    for dependency in bpy.utils.blend_paths(absolute=True, local=False):
        if not os.path.exists(dependency):
            logging.warn("Missing file {} for blend {}".format(dependency, in_filename))
        if os.path.getmtime(out_filename) > last_build_time:
            build = True
    return build
    

def get_instructions(filename, export_type):
    if export_type not in FUNCT_DICT:
        raise Exception("Unable to export {}: unknown export type".format(filename))
    function, extension = FUNCT_DICT[export_type]
    return function, filename.replace('.blend', extension)
        


def main(args):
    """Searches for blend files and exports them"""
    parser = argparse.ArgumentParser("Converts lots of blend files")
    parser.add_argument('--projectdir', required=True, help="Base directory to search for blend files inside of")
    parser.add_argument('--outfile', default=None, help="A log of all output files is put in here. This is useful to automate VCS ignore files")
    config = parser.parse_args(args)

    outfiles = []

    for folder, subfolders, filenames in os.walk(config.projectdir):
        for filename in filenames:
            if filename.endswith('.blend'):
                outfiles += export_blend(config.projectdir, os.path.join(folder, filename))

    if config.outfile is not None:
        logging.info("Outputting built files list to {}".format(config.outfile))
        outfiles = [os.path.relpath(f, os.path.dirname(config.outfile)) for f in outfiles]
        with open(config.outfile, 'w') as outfile:
            outfile.write("\n".join(outfiles))
        
    

def run_function_with_args(function):
    '''Finds the args to the python script rather than to blender as a whole
    '''
    arg_pos = sys.argv.index('--') + 1
    function(sys.argv[arg_pos:])
    print("SUCCESS")
    sys.exit(0)


run_function_with_args(main)
