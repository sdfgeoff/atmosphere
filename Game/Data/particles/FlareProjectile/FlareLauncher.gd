extends Spatial

const REFIRE_TIME = 1.0  # Time for flare launcher to reload
const Projectile = preload("res://Data/particles/FlareProjectile/FlareProjectile.tscn")
var refire_timer = Timer.new()

var material = null  # Flare material has to be made unique so we can fade it in
var initial_light_energy = 0.0  # So we know how bright the light should be


func _ready():
	refire_timer.one_shot = true
	refire_timer.wait_time = REFIRE_TIME
	self.add_child(refire_timer)
	
	var gfx_mesh = self.find_node('FlareGFX')
	self.material = gfx_mesh.mesh.surface_get_material (0).duplicate()
	gfx_mesh.set_surface_material(0, self.material)
	
	self.initial_light_energy = $FlareLight.light_energy
	
	
		
func can_fire():
	return self.refire_timer.time_left <= 0


func _process(delta):
	var percent = 1.0 - (self.refire_timer.time_left / REFIRE_TIME)
	
	# Fade in the flare light and gfx mesh
	var vis_fade = pow(percent, 2.2)
	self.material.set_shader_param('fade', vis_fade)
	$FlareLight.light_energy = vis_fade * self.initial_light_energy
	
	# Spin fast when "generating" a flare
	self.rotate_z(delta / (percent + 0.01))


func fire():
	if self.can_fire():
		var new_projectile = Projectile.instance()
		self.get_tree().get_root().add_child(new_projectile)
		new_projectile.start_motion(self.global_transform)
		self.refire_timer.start()
