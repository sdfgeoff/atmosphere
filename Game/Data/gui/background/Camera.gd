extends Camera

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export var FORWARD_SPEED = 1.0
export var ROTATE_SPEED = 0.05
export var NOISE_SIZE = 0.005
export var RETURN_AMOUNT = 0.001

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	self.translation.y -= delta * FORWARD_SPEED
	self.rotate_y(delta * ROTATE_SPEED)
	if self.translation.y < -10:
		self.translation.y += 10
		
	self.translation.x += (randf() - 0.5) * NOISE_SIZE
	self.translation.z += (randf() - 0.5) * NOISE_SIZE
	self.translation.x -= self.translation.x * RETURN_AMOUNT
	self.translation.z -= self.translation.z * RETURN_AMOUNT
	
