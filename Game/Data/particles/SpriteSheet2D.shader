shader_type canvas_item;
render_mode blend_add;

uniform vec4 tint : hint_color;
uniform float fade : hint_range(0, 1);

void fragment(){
	vec4 col = texture(TEXTURE,UV);
	COLOR = (col * tint + col) * fade;
}