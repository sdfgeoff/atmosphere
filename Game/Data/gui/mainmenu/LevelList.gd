extends Node2D

const PATHS = [
	"res://Levels",
	"user://Levels"
]

const LEVELDATA_FILENAME = "leveldata.json"

var all_levels = []

func load_leveldata(level_folder):
	"""Returns a dict of the level data or null"""
	var fullpath = level_folder + '/' + LEVELDATA_FILENAME
	var fileraw = File.new()
	if fileraw.open(fullpath, fileraw.READ) == OK:
		var filedata = JSON.parse(fileraw.get_as_text())
		if filedata.error != OK:
			print("Error parsing %s: %s" % [fullpath, filedata.error_string])
		else:
			if not "name" in filedata.result:
				filedata.result["name"] = "UN-NAMED"
			if not "icon" in filedata.result:
				filedata.result["icon"] = null
			return filedata.result

func scan_directory(dir):
	"""Searches for subfolders containing LEVELDATA_FILENAME"""
	print("Searching for levels in: ", dir)
	var search_dir = Directory.new()
	if search_dir.change_dir(dir) != OK:
		search_dir.make_dir(dir)
		return []

	var fullpath = ""
	
	search_dir.list_dir_begin(true, true)
	var level_data = null
	var levels = []
	
	while(1):
		var filename = search_dir.get_next()
		if filename == "":
			# Reached last folder
			break
		if search_dir.current_is_dir():
			fullpath = dir + '/' + filename
			level_data = load_leveldata(fullpath)
			if level_data != null:
				print("Found level at: ", fullpath)
				levels.append([level_data['name'], level_data['icon'], fullpath])
	
	return levels

func rescan_directories():
	"""Searches for levels"""
	self.all_levels = []
	$LevelList.clear()
	
	for dir in PATHS:
		all_levels += scan_directory(dir)
	
	for level in self.all_levels:
		if level[1] != null:
			var icon = ImageTexture.new()
			icon.load(level[2] + '/' + level[1])
			$LevelList.add_item(level[0], icon)
		else:
			$LevelList.add_item(level[0])


func _on_select_changed(index):
	var base_directory = self.all_levels[index][2]
	var data = self.load_leveldata(base_directory)
	
	"""var tex = ImageTexture.new()
	if "preview" in data:
		tex.load(base_directory + '/' + data["preview"])
	$Preview.texture = tex
	
	if "description" in data:
		$LevelDescription.text = data["description"]
	else:
		$LevelDescription.text = ""
	"""


func _ready():
	self.rescan_directories()
	$LevelList.connect("item_selected", self, "_on_select_changed")
	
	$LevelList.select(0)
	self._on_select_changed(0)