extends RigidBody

const SPEED = 5.0

func start_motion(transform):
	self.global_transform = transform
	self.linear_velocity = transform.basis * Vector3(0, 0, -SPEED)
	#self.linear_velocity += launcher_velocity