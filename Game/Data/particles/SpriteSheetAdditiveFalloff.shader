shader_type spatial;
render_mode blend_add,depth_draw_opaque,cull_disabled,unshaded;
uniform vec4 color : hint_color;
uniform float fade = 1.0;
uniform sampler2D texture_albedo : hint_albedo;

varying float factor;

void vertex(){
	vec4 vert_world = WORLD_MATRIX * vec4(VERTEX, 1.0);
	vec3 cam_local = (INV_CAMERA_MATRIX * vert_world).xyz;
	cam_local = normalize(cam_local);
	vec3 cam_normal = (MODELVIEW_MATRIX * vec4(NORMAL, 0.0)).xyz;
	factor = pow(dot(cam_normal, cam_local), 2.0);
}

void fragment() {
	vec4 albedo_tex = texture(texture_albedo,UV);
	ALBEDO = fade * factor * (color.rgb * albedo_tex.rgb + albedo_tex.rgb);
}
