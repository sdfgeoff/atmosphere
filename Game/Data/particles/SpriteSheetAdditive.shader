shader_type spatial;
render_mode blend_add,depth_draw_opaque,cull_disabled,unshaded;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;



void fragment() {
	vec4 albedo_tex = texture(texture_albedo,UV);
	ALBEDO = albedo.rgb * albedo_tex.rgb + albedo_tex.rgb;
}
