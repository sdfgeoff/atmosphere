Levels are discovered as folders containing a "leveldata.json" file. This file contains information about the level including:
 - It's name
 - Author
 - Description
 - Path to an icon file
 - Path to a preview image
 - Path to the main scene file

An icon should be a:
 - png or jpb
 - it is displayed at 32px resolution, so no need to make it higher res.

The preview image should be 512x512px.

Items are placed in-level by linking in the scene. These linkable items include:
 - Doors
 - Enemies
 - The player spawn
 - Powerups

 Both res://Levels/ and user://Levels/ are searched, allowing downloadable levels.
