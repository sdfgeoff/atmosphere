extends RigidBody

export (float) var LIN_THRUST = 20
export (float) var LIN_DRAG = 4
export (float) var ANG_THRUST = 2
export (float) var ANG_DRAG = 1.5

const LIN_DIRECTION_MAP = {
	'strafe_forwards':Vector3(0, 0, -1),
	'strafe_backwards':Vector3(0, 0, 1),
	'strafe_left':Vector3(-1, 0, 0),
	'strafe_right':Vector3(1, 0, 0),
	'strafe_up':Vector3(0, 1, 0),
	'strafe_down':Vector3(0, -1, 0)
}

var MOUSE_SETTINGS = {
	'x_gain':0.3,
	'y_gain':0.3,
}

const ANG_DIRECTION_MAP = {
	'pan_left':Vector3(0, 1, 0),
	'pan_right':Vector3(0, -1, 0),
	'rotate_up':Vector3(1, 0, 0),
	'rotate_down':Vector3(-1, 0, 0),
	'roll_left':Vector3(0, 0, 1),
	'roll_right':Vector3(0, 0, -1),
}


var mouse_speed = Vector2(0, 0)

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	print("Creating Player Ship")
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	
	mouse_speed.x = 0
	mouse_speed.y = 0
	

func _input(event):
	if(event is InputEventMouseMotion):
		self.mouse_speed.x = -event.relative.y * MOUSE_SETTINGS.x_gain
		self.mouse_speed.y = -event.relative.x * MOUSE_SETTINGS.y_gain


func _process(delta):
	# Linear Speed
	if Input.is_action_just_pressed("launch_flare"):
		$FlareLaunchers.fire()
	var transf = global_transform.basis
	
	var lin_thrust = Vector3(0, 0, 0)
	for event_name in LIN_DIRECTION_MAP:
		if Input.is_action_pressed(event_name):
			lin_thrust += LIN_DIRECTION_MAP[event_name]
	
	box_clamp_vector(lin_thrust)
	lin_thrust *= LIN_THRUST
	
	var lin_drag = linear_velocity * LIN_DRAG
	var lin_output = (transf * lin_thrust - lin_drag) * delta
	
	apply_impulse(Vector3(0, 0, 0), lin_output)
	
	# Angular Speed
	var ang_thrust = Vector3(0, 0, 0)
	for event_name in ANG_DIRECTION_MAP:
		if Input.is_action_pressed(event_name):
			ang_thrust += ANG_DIRECTION_MAP[event_name]
	ang_thrust.x += self.mouse_speed.x
	ang_thrust.y += self.mouse_speed.y
	self.mouse_speed.x = 0
	self.mouse_speed.y = 0
	
	ang_thrust = box_clamp_vector(ang_thrust)
	ang_thrust *= ANG_THRUST
	
	var ang_drag = angular_velocity * ANG_DRAG
	var ang_output = (transf * ang_thrust - ang_drag) * delta
	
	apply_torque_impulse(ang_output)

static func box_clamp_vector(vec):
	vec.x = clamp(vec.x, -1, 1)
	vec.y = clamp(vec.y, -1, 1)
	vec.z = clamp(vec.z, -1, 1)
	return vec
